package com.gojolo.zekimisin;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Random;

public class TestStart extends AppCompatActivity {
    Integer getid,sira;
    CountDownTimer cnt;
    TextView time;
    ArrayList<String> soru;
    TextView sorutext,result;
    ImageView img0,img1,img2,img3,img4;
    LinearLayout soruekran,testcevap;
    int sayac=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_start);
        getid= getIntent().getExtras().getInt("id",0);
        sira= getIntent().getExtras().getInt("sira",0);
        time =(TextView)findViewById(R.id.time);
        sorutext =(TextView)findViewById(R.id.sorutext);
        result=(TextView)findViewById(R.id.result);
        img0 =(ImageView)findViewById(R.id.img0);
        img1 =(ImageView)findViewById(R.id.img1);
        img2 =(ImageView)findViewById(R.id.img2);
        img3 =(ImageView)findViewById(R.id.img3);
        img4 =(ImageView)findViewById(R.id.img4);
        soruekran=(LinearLayout)findViewById(R.id.soruekran);
        testcevap=(LinearLayout)findViewById(R.id.testcevap);
        gamestart();
        cnt= new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                if(String.valueOf(millisUntilFinished / 1000).equals("6")){
                    soruekran.setVisibility(View.GONE);
                    sorutext.setText(soru.get(1));
                }
                time.setText(String.valueOf(millisUntilFinished / 1000));
            }
            public void onFinish() {
                sayac++;
            cnt.start();
            soruekran.setVisibility(View.VISIBLE);
            gamestart();
            }

        }.start();
    }
    public void testControl(String  i){
        testcevap.setVisibility(View.VISIBLE);
        if(soru.get(7).equals(i)) {
            result.setText("Doğru");
         }
         else{
            result.setText("Yanlış");
        }
        }
        public void gamestart(){
            testcevap.setVisibility(View.GONE);
            AllQuestion all = new AllQuestion();
            switch (getid){
                case 0:{
                    switch (sira){
                        case 0:{     soru=all.sekilverenk(sayac); break;}
                        case 1:{ break;}
                        case 2:{ break;}
                        case 3:{ break;}
                        case 4:{ break;}
                        case 5:{ break;}
                        case 6:{ break;}
                        case 7:{ break;}
                    }
                    break;
                }
                case 1:{
                    switch (sira){
                        case 0:{ break;}
                        case 1:{ break;}
                        case 2:{ break;}
                        case 3:{ break;}
                        case 4:{ break;}
                        case 5:{ break;}
                        case 6:{ break;}
                        case 7:{ break;}
                    }
                    break;
                }
                case 2:{
                    switch (sira){
                        case 0:{ break;}
                        case 1:{ break;}
                        case 2:{ break;}
                        case 3:{ break;}
                        case 4:{ break;}
                        case 5:{ break;}
                        case 6:{ break;}
                        case 7:{ break;}
                    }
                    break;
                }
                case 3:{
                    switch (sira){
                        case 0:{ break;}
                        case 1:{ break;}
                        case 2:{ break;}
                        case 3:{ break;}
                        case 4:{ break;}
                        case 5:{ break;}
                        case 6:{ break;}
                        case 7:{ break;}
                    }
                    break;
                }
                case 4:{
                    switch (sira){
                        case 0:{ break;}
                        case 1:{ break;}
                        case 2:{ break;}
                        case 3:{ break;}
                        case 4:{ break;}
                        case 5:{ break;}
                        case 6:{ break;}
                        case 7:{ break;}
                    }
                    break;
                }
            }

            sorutext.setText(soru.get(0));
            Picasso.get().load(soru.get(2)).into(img0);
            Picasso.get().load(soru.get(3)).into(img1);
            Picasso.get().load(soru.get(4)).into(img2);
            Picasso.get().load(soru.get(5)).into(img3);
            Picasso.get().load(soru.get(6)).into(img4);
            img1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    testControl("1");
                }
            });
            img2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    testControl("2");
                }
            });
            img3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    testControl("3");
                }
            });
            img4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    testControl("4");
                }
            });
        }
}
