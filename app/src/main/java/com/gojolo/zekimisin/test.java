package com.gojolo.zekimisin;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;

public class test extends Activity {
    Integer getid,sira;
    ImageView geri,ileri;
    Intent in;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        geri =(ImageView)findViewById(R.id.geri);
        ileri =(ImageView)findViewById(R.id.ileri);
        geri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                in =new Intent(test.this,Home.class);
                startActivity(in);
            }
        });
        getid= getIntent().getExtras().getInt("id",0);
        sira= getIntent().getExtras().getInt("sira",0);
        ileri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                in = new Intent(test.this,TestStart.class);
                in.putExtra("id", getid);
                in.putExtra("sira", sira);
                startActivity(in);
            }
        });
 }
    @Override
    public void onBackPressed(){
    }
}
