package com.gojolo.zekimisin;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommentAdapter extends BaseAdapter{
    private Context mContext;
    List<String> xyorum = new ArrayList<String>();
    List<String> xtarih = new ArrayList<String>();
    List<String> xprofil = new ArrayList<String>();
    List<String> xpuan = new ArrayList<String>();
    List<String> xusername = new ArrayList<String>();
    public int who;

    public CommentAdapter(Context c,List<String> xyorum,List<String> xtarih,List<String> xusername,List<String> xpuan,List<String> xprofil) {
        mContext = c;
        this.xyorum = xyorum;
        this.xprofil=xprofil;
        this.xtarih=xtarih;
        this.xpuan=xpuan;
        this.xusername=xusername;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return xyorum.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return xyorum.indexOf(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid=convertView;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            grid = inflater.inflate(R.layout.grid_comment, null);

        }
        ImageView photo = (ImageView)grid.findViewById(R.id.photo);
        TextView score = (TextView) grid.findViewById(R.id.score);
        TextView usernametext = (TextView) grid.findViewById(R.id.username);
        TextView mesajtext = (TextView) grid.findViewById(R.id.mesajtext);
        TextView tarihtext = (TextView) grid.findViewById(R.id.tarihtext);
        Log.e("profil",xprofil.get(position));
        switch (xprofil.get(position)){
            case "1":{ photo.setImageResource(R.drawable.ph1); break;}
            case "2":{ photo.setImageResource(R.drawable.ph2); break;}
            case "3":{ photo.setImageResource(R.drawable.ph3); break;}
            case "4":{ photo.setImageResource(R.drawable.ph4); break;}
            case "5":{ photo.setImageResource(R.drawable.ph5); break;}
            case "6":{ photo.setImageResource(R.drawable.ph6); break;}
            case "7":{ photo.setImageResource(R.drawable.ph7); break;}
            case "8":{ photo.setImageResource(R.drawable.ph8); break;}
            case "9":{ photo.setImageResource(R.drawable.ph9); break;}
            case "10":{ photo.setImageResource(R.drawable.ph10); break;}
        }
        usernametext.setText(xusername.get(position));
        score.setText(xpuan.get(position));
        mesajtext.setText(xyorum.get(position));
        tarihtext.setText(xtarih.get(position));
        return grid;
    }
}