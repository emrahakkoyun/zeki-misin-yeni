package com.gojolo.zekimisin;

class Comm {
    public String yorum;
    public String tarih;
    public String username;
    public String puan;
    public String profil;
    public String feedkey;
    public Comm(){

    }


    public String getYorum() {
        return yorum;
    }
    public String getTarih() {
        return tarih;
    }
    public String getUsername() {
        return username;
    }

    public String getPuan() {
        return puan;
    }
    public String getProfil() {
        return profil;
    }

    public String getFeedkey() {
        return feedkey;
    }

    public Comm(String yorum, String tarih, String username, String puan, String profil, String feedkey){
        this.yorum=yorum;
        this.tarih=tarih;
        this.username=username;
        this.puan=puan;
        this.profil=profil;
        this.feedkey=feedkey;
    }
}
