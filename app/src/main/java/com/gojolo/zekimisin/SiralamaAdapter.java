package com.gojolo.zekimisin;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SiralamaAdapter extends BaseAdapter{
    private Context mContext;
    List<String> fprofil = new ArrayList<String>();
    List<String> fpuan = new ArrayList<String>();
    List<String> fusername = new ArrayList<String>();
    List<String> fsiralama = new ArrayList<String>();
    public int who;

    public SiralamaAdapter(Context c,List<String> fprofil,List<String> fusername,List<String> fpuan,List<String> fsiralama) {
        mContext = c;
        this.fprofil = fprofil;
        this.fusername=fusername;
        this.fpuan=fpuan;
        this.fsiralama=fsiralama;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return fprofil.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return fprofil.indexOf(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid=convertView;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            grid = inflater.inflate(R.layout.grid_siralama, null);

        }
        ImageView photo = (ImageView)grid.findViewById(R.id.photo);
        TextView score = (TextView) grid.findViewById(R.id.score);
        TextView usernametext = (TextView) grid.findViewById(R.id.username);
        TextView siralar = (TextView) grid.findViewById(R.id.siralar);
        Log.e("profil",fprofil.get(position));
        switch (fprofil.get(position)){
            case "1":{ photo.setImageResource(R.drawable.ph1); break;}
            case "2":{ photo.setImageResource(R.drawable.ph2); break;}
            case "3":{ photo.setImageResource(R.drawable.ph3); break;}
            case "4":{ photo.setImageResource(R.drawable.ph4); break;}
            case "5":{ photo.setImageResource(R.drawable.ph5); break;}
            case "6":{ photo.setImageResource(R.drawable.ph6); break;}
            case "7":{ photo.setImageResource(R.drawable.ph7); break;}
            case "8":{ photo.setImageResource(R.drawable.ph8); break;}
            case "9":{ photo.setImageResource(R.drawable.ph9); break;}
            case "10":{ photo.setImageResource(R.drawable.ph10); break;}
        }
        usernametext.setText(fusername.get(position));
        score.setText(fpuan.get(position));
        siralar.setText(fsiralama.get(position));
        return grid;
    }
}