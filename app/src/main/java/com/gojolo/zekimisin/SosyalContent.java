package com.gojolo.zekimisin;

import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class SosyalContent extends Fragment {

    View view;
    List<String> xyorum = new ArrayList<String>();
    List<String> xtarih = new ArrayList<String>();
    List<String> xprofil = new ArrayList<String>();
    List<String> xpuan = new ArrayList<String>();
    List<String> xusername = new ArrayList<String>();
    List<String> xfeedkey = new ArrayList<String>();
    ImageButton gonder;
    EditText yorumyaz;
    FirebaseDatabase db;
    String fphoto;
    String fpuan;
    String fusername;
    String deviceId;
    String comment;
    String like;
    TextView likecounttext;
    TextView commentcounttext;
    int likedurum;
    GridView grid;
    String feedkey;
    CommentAdapter adapter;
    public static SosyalContent newInstance() {
        SosyalContent fragment = new SosyalContent();
        return fragment;
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_content, container, false);
        gonder =(ImageButton)view.findViewById(R.id.gonder);
        yorumyaz=(EditText)view.findViewById(R.id.yorumyaz);
        grid=(GridView)view.findViewById(R.id.grid);
        db= FirebaseDatabase.getInstance();
        if (getArguments() != null) {
        String mesaj = getArguments().getString("mesaj");
        String profil = getArguments().getString("profil");
        String tarih = getArguments().getString("tarih");
        String resim = getArguments().getString("resim");
        String username = getArguments().getString("username");
        String puan = getArguments().getString("puan");
        comment = getArguments().getString("comment");
        like = getArguments().getString("like");
            deviceId= Settings.Secure.getString(getActivity().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            feedkey= getArguments().getString("feedkey");
            ImageView photo = (ImageView)view.findViewById(R.id.photo);
            ImageView picture = (ImageView)view.findViewById(R.id.picture);
            ImageView likeclc = (ImageView)view.findViewById(R.id.like);
            TextView score = (TextView) view.findViewById(R.id.score);
            TextView usernametext = (TextView) view.findViewById(R.id.username);
            TextView mesajtext = (TextView) view.findViewById(R.id.mesajtext);
            TextView tarihtext = (TextView) view.findViewById(R.id.tarihtext);
            commentcounttext = (TextView) view.findViewById(R.id.commentcount);
            likecounttext = (TextView)view.findViewById(R.id.likecount);
            switch (profil){
                case "1":{ photo.setImageResource(R.drawable.ph1); break;}
                case "2":{ photo.setImageResource(R.drawable.ph2); break;}
                case "3":{ photo.setImageResource(R.drawable.ph3); break;}
                case "4":{ photo.setImageResource(R.drawable.ph4); break;}
                case "5":{ photo.setImageResource(R.drawable.ph5); break;}
                case "6":{ photo.setImageResource(R.drawable.ph6); break;}
                case "7":{ photo.setImageResource(R.drawable.ph7); break;}
                case "8":{ photo.setImageResource(R.drawable.ph8); break;}
                case "9":{ photo.setImageResource(R.drawable.ph9); break;}
                case "10":{ photo.setImageResource(R.drawable.ph10); break;}
            }
            if(resim==null){}else{
                Picasso.get().load(resim).resize(600, 300).into(picture);}
            usernametext.setText(username);
            score.setText(puan);
            mesajtext.setText(mesaj);
            tarihtext.setText(tarih);
            likecomm();
            DatabaseReference dbRefYeni =db.getReference("user/"+deviceId);
            dbRefYeni.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Map<String , String> mapa = (Map)dataSnapshot.getValue();
                    fphoto=String.valueOf(mapa.get("profil"));
                    fpuan=String.valueOf(mapa.get("puan"));
                    fusername=String.valueOf(mapa.get("username"));
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });

            DatabaseReference myRef =db.getReference().child("Like");
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Map<String , String> mapa = (Map)dataSnapshot.getValue();
                    if(mapa!=null) {
                        if (String.valueOf(mapa.get("feedkey")).equals(feedkey) && String.valueOf(mapa.get("deviceid")).equals(deviceId)) {
                            likedurum = 1;
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
            gonder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String yorum= yorumyaz.getText().toString();
                    if(yorum==null){
                        Toast.makeText(getActivity(),"Bir şeyler yazın",Toast.LENGTH_LONG).show();
                    }else{
                    mesajgonder(yorum,feedkey,fphoto,fpuan,fusername,comment);
                    yorumyaz.setText("");
                    }
                }
            });
            likeclc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(likedurum!=1){
                        likeartir(feedkey);
                    }
                }
            });

        }
        DatabaseReference myRef =db.getReference().child("Comment");
        myRef.orderByKey().limitToLast(60).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Comm newPost = dataSnapshot.getValue(Comm.class);
                if(String.valueOf(feedkey).equals(String.valueOf(newPost.getFeedkey()))) {
                    xyorum.add(String.valueOf(newPost.getYorum()));
                    xtarih.add(String.valueOf(newPost.getTarih()));
                    xusername.add(String.valueOf(newPost.getUsername()));
                    xpuan.add(String.valueOf(newPost.getPuan()));
                    xprofil.add(String.valueOf(newPost.getProfil()));
                    adapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        adapter = new CommentAdapter(getActivity(), xyorum,xtarih,xusername,xpuan,xprofil);
        grid.setAdapter(adapter);
        return view;
    }
    public void mesajgonder(String yorum,String feedkey,String fphoto,String fpuan,String fusername,String commentcount){
        DatabaseReference dbRef=db.getReference("Comment");
        String key= dbRef.push().getKey();
        DatabaseReference dbRefYenixx=db.getReference("Comment/"+key+"/feedkey");
        dbRefYenixx.setValue(feedkey);
        DatabaseReference dbRefYenixy=db.getReference("Comment/"+key+"/yorum");
        dbRefYenixy.setValue(yorum);
        DatabaseReference dbRefYenia=db.getReference("Comment/"+key+"/profil");
        dbRefYenia.setValue(fphoto);
        DatabaseReference dbRefYenib=db.getReference("Comment/"+key+"/puan");
        dbRefYenib.setValue(fpuan);
        DatabaseReference dbRefYenic=db.getReference("Comment/"+key+"/username");
        dbRefYenic.setValue(fusername);
        DatabaseReference dbRefYenix=db.getReference("Comment/"+key+"/tarih");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String strDate = dateFormat.format(date).toString();
        dbRefYenix.setValue(strDate);
        DatabaseReference dbR=db.getReference("sosyal");
        DatabaseReference dbRefYenip=db.getReference("sosyal/"+feedkey+"/comment");
        comment=String.valueOf(Integer.parseInt(commentcount)+1);
        likecomm();
        dbRefYenip.setValue(comment);
    }
    public void likeartir(String feedkey){
        DatabaseReference dbRef=db.getReference("Like");
        DatabaseReference dbRefYenixx=db.getReference("Like/feedkey");
        dbRefYenixx.setValue(feedkey);
        DatabaseReference dbRefYenic=db.getReference("Like/deviceid");
        dbRefYenic.setValue(deviceId);
        DatabaseReference dbRefYenip=db.getReference("sosyal/"+feedkey+"/like");
        like=String.valueOf(Integer.parseInt(like)+1);
        likecomm();
        dbRefYenip.setValue(like);
    }
   public void likecomm(){
       commentcounttext.setText(comment);
       likecounttext.setText(like);
   }

}
