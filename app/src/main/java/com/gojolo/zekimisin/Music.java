package com.gojolo.zekimisin;

import android.content.Context;
import android.media.MediaPlayer;

public class Music {
    static MediaPlayer mp;
    public static boolean isplayingMusic=false;
    public static void start(Context context, int music)
    {
        mp=MediaPlayer.create(context,music);
        // mp.prepareAsync();
        if(!mp.isPlaying())
        {
            isplayingMusic=true;
            mp.start();
        }
    }
    public static void stop()
    {
        isplayingMusic=false;
        mp.stop();
    }
}