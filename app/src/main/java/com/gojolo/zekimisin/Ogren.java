package com.gojolo.zekimisin;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

public class Ogren extends Fragment {
    Intent in;
    public Integer[] resimler = {
            R.drawable.login,
            R.drawable.login,
            R.drawable.login,
            R.drawable.login,
            R.drawable.login,
            R.drawable.login,
            R.drawable.login,
            R.drawable.login,

    };
    public String[] hafiza = {
            "Şekiller ve Renkler",
            "Desen Anımsama",
            "Görsel Anımsama",
            "Rakamları Anımsa",
            "Kuş Sesleri",
            "Renkli Lambalar",
            "Çoklu Çalışma",
            "Harita Anımsama",
    };
    public String[] dikkat = {
            "Farklı Resim Bul",
            "Eşini Bul",
            "Aynalama",
            "Silüeti Bul",
            "Peş Peşe",
            "Renk Desen Şekil",
            "Gölgeyi Bul",
            "Görsel Dikkat Eğitimi"
    };
    public String[] mantik = {
            "Renk Karmaşası",
            "Hangisi Daha Ağır",
            "Küp Sayısı",
            "Sayıları Sırala",
            "Hareketli Toplar",
            "Eşitliği Sağla",
            "Dört İşlem",
            "Zaman yönetimi"
    };
    public String[] gorsel = {
            "Görsel Şekiller",
            "Kuleler",
            "Renk Çoğunluğu",
            "Hafıza Kartları",
            "Döndür Bul",
            "Eksik Parçayı Bul",
            "Bakış Açısı",
            "Sağ el , Sol el"

    };
    public String[] sozel={
            "Görsel Tanımlama",
            "Resim Tanıma",
            "İlişkiyi Bul",
            "Eş anlam Zıt anlam",
            "Sınıflandırma",
            "Kelimeyi tanımlama",
            "Kelimeyi bul",
            "Adlandırma"
    };
    public String[] yazilar = {
            "Hafıza Geliştir",
            "Dikkat Geliştir",
            "Mantık Geliştir",
            "Görsel Geliştir",
            "Sözel Zeka Geliştir",
    };
    View view;
    int x=0,y;
    GridView grid;
    ImageAdapter adapter;
    Intent intent;
    public static Ogren newInstance() {
        Ogren fragment = new Ogren();
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ogren, container, false);

        adapter = new ImageAdapter(getActivity(), yazilar, resimler,1);
        grid=(GridView)view.findViewById(R.id.grid);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                x++;
                if(x<2) {
                    switch (position) {
                        case 0: {
                            y=0;
                            adapter = new ImageAdapter(getActivity(), hafiza, resimler,1);
                            grid.setAdapter(adapter);
                            break;
                        }
                        case 1: {
                            y=1;
                            adapter = new ImageAdapter(getActivity(), dikkat, resimler,1);
                            grid.setAdapter(adapter);
                            break;
                        }
                        case 2: {
                            y=2;
                            adapter = new ImageAdapter(getActivity(), mantik, resimler,1);
                            grid.setAdapter(adapter);
                            break;
                        }
                        case 3: {
                            y=3;
                            adapter = new ImageAdapter(getActivity(), gorsel, resimler,1);
                            grid.setAdapter(adapter);
                            break;
                        }
                        case 4: {
                            y=4;
                            adapter = new ImageAdapter(getActivity(), sozel, resimler,1);
                            grid.setAdapter(adapter);
                            break;
                        }
                    }
                }
                else{
                    inrequestadd(position,y);
                }

            }
        });

        return view;
    }
    public void inrequestadd(final int position,final int sira) {
        intent = new Intent(getActivity(),test.class);
        intent.putExtra("id", position);
        intent.putExtra("sira", sira);
        startActivity(intent);
    }
}
