package com.gojolo.zekimisin;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.codemybrainsout.onboarder.AhoyOnboarderActivity;
import com.codemybrainsout.onboarder.AhoyOnboarderCard;

import java.util.ArrayList;
import java.util.List;

public class BeforeHome extends AhoyOnboarderActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AhoyOnboarderCard ahoyOnboarderCard1 = new AhoyOnboarderCard("Egzersiz Yapın", "Egzersiz yaparak kendinizi yarışmaya hazırlayın", R.drawable.mental);
        AhoyOnboarderCard ahoyOnboarderCard2 = new AhoyOnboarderCard("Zeka Soruları", "Zor Zeka sorularını kısa sürede çözün", R.drawable.question);
        AhoyOnboarderCard ahoyOnboarderCard3 = new AhoyOnboarderCard("Yarışma", "Rakipleriniz hazırlıklı ve sizinle yarışma hazır durumdalar", R.drawable.kupa);
        ahoyOnboarderCard1.setBackgroundColor(R.color.white);
        ahoyOnboarderCard2.setBackgroundColor(R.color.white);
        ahoyOnboarderCard3.setBackgroundColor(R.color.white);

        List<AhoyOnboarderCard> pages = new ArrayList<>();

        pages.add(ahoyOnboarderCard1);
        pages.add(ahoyOnboarderCard2);
        pages.add(ahoyOnboarderCard3);

        for (AhoyOnboarderCard page : pages) {
            page.setTitleColor(R.color.black);
            page.setDescriptionColor(R.color.grey_600);
        }

        setFinishButtonTitle("Devam et");
        showNavigationControls(false);

        List<Integer> colorList = new ArrayList<>();
        colorList.add(R.color.solid_one);
        colorList.add(R.color.solid_two);
        colorList.add(R.color.solid_three);

        setColorBackground(colorList);

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        setFont(face);

        setOnboardPages(pages);
    }
    @Override
    public void onFinishButtonPressed() {
        Intent in = new Intent(BeforeHome.this,Home.class);
        startActivity(in);
        finish();
    }
}
