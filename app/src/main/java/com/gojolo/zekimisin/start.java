package com.gojolo.zekimisin;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.codemybrainsout.onboarder.AhoyOnboarderActivity;
import com.codemybrainsout.onboarder.AhoyOnboarderCard;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;

public class start extends  AppCompatActivity{

    Music ms = new Music();
    CountDownTimer cnt;
    String deviceId;
    FirebaseDatabase db;
    ConstraintLayout constraintLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_start);
        AllQuestion all = new AllQuestion();
        ms.start(getApplicationContext(),R.raw.music);
        FirebaseApp.initializeApp(this);
        db=FirebaseDatabase.getInstance();
        deviceId= Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        constraintLayout = (ConstraintLayout) findViewById(R.id.cons) ;
        if(!isNetworkAvailable(this)){
            Snackbar snackbar = Snackbar.make(constraintLayout, "İnternet Bağlantısını Kontrol Ediniz", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
      cnt= new CountDownTimer(1000, 1000) {

            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
               yonlendir();
            }

        }.start();
    }
    public void onStop(){
        cnt.cancel();
        finish();
        super.onStop();
    }
    public void yonlendir(){
        DatabaseReference dbRefYeni =db.getReference("user");
        dbRefYeni.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> keys=dataSnapshot.getChildren();
                for(DataSnapshot key:keys)
                {
                    if(key.getKey().equals(deviceId)){
                        gonder(1);
                    }
                    else{
                        gonder(0);
                    }
                }
                if (!dataSnapshot.exists()) {
                    gonder(0);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    public void gonder(int i){
        if(i==1){
            Intent in = new Intent(start.this,Home.class);
            startActivity(in);
            finish();
        }
        else{
            Intent in = new Intent(start.this,Kayit.class);
            startActivity(in);
            finish();
        }
    }
    public boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
