package com.gojolo.zekimisin;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Sosyal extends Fragment {

    View view;
    private FragmentActivity myContext;
    private FloatingActionButton fab;
    List<String> mesaj = new ArrayList<String>();
    List<String> resim = new ArrayList<String>();
    List<String> tarih = new ArrayList<String>();
    List<String> profil = new ArrayList<String>();
    List<String> puan = new ArrayList<String>();
    List<String> username = new ArrayList<String>();
    List<String> feedkey = new ArrayList<String>();
    List<String> comment = new ArrayList<String>();
    List<String> like = new ArrayList<String>();
    Map<String , String> mapa;
    SosyalAdapter sosadapter;
    FirebaseDatabase db;
    GridView grid;
    int i=0;
    public static Sosyal newInstance() {
        Sosyal fragment = new Sosyal();
        return fragment;
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sosyal, container, false);
        grid=(GridView)view.findViewById(R.id.grid);
        db= FirebaseDatabase.getInstance();
        DatabaseReference myRef =db.getReference().child("sosyal");
        myRef.orderByKey().limitToLast(60).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Post newPost = dataSnapshot.getValue(Post.class);
                mesaj.add(String.valueOf(newPost.getMesaj()));
                tarih.add(String.valueOf(newPost.getTarih()));
                resim.add(String.valueOf(newPost.getResim()));
                username.add(String.valueOf(newPost.getUsername()));
                puan.add(String.valueOf(newPost.getPuan()));
                profil.add(String.valueOf(newPost.getProfil()));
                like.add(String.valueOf(newPost.getLike()));
                comment.add(String.valueOf(newPost.getComment()));
                feedkey.add(String.valueOf(dataSnapshot.getKey()));
                sosadapter.notifyDataSetChanged();
                grid.smoothScrollToPosition(mesaj.size()-1);
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        sosadapter = new SosyalAdapter(getActivity(), mesaj, resim, tarih, profil, puan, username,comment,like);
        grid.setAdapter(sosadapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SosyalContent fragmentb = new SosyalContent();
                Bundle args = new Bundle();
                args.putString("mesaj",mesaj.get(position));
                args.putString("profil",profil.get(position));
                args.putString("tarih",tarih.get(position));
                args.putString("resim",resim.get(position));
                args.putString("username",username.get(position));
                args.putString("puan",puan.get(position));
                args.putString("comment",comment.get(position));
                args.putString("like",like.get(position));
                args.putString("feedkey",feedkey.get(position));
                fragmentb.setArguments(args);
                          loadFragment(fragmentb);
            }
        });
        fab = (FloatingActionButton)view.findViewById(R.id.fabi);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             shownewpost();
            }
        });
        return view;
    }
    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }
    private void shownewpost() {
        FragmentManager fm = myContext.getSupportFragmentManager();
        newpost newpos = new newpost();
        newpos.show(fm, "fragment_edit_name");
    }
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
