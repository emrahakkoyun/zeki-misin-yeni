package com.gojolo.zekimisin;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Map;

public class AllQuestion {
    ArrayList<String> soruresmi = new ArrayList<String>();
    ArrayList<String> iste = new ArrayList<String>();
    ArrayList<String> soru = new ArrayList<String>();
    ArrayList<String> sik1 = new ArrayList<String>();
    ArrayList<String> sik2 = new ArrayList<String>();
    ArrayList<String> sik3 = new ArrayList<String>();
    ArrayList<String> sik4 = new ArrayList<String>();
    ArrayList<String> cevap = new ArrayList<String>();
    ArrayList<ArrayList<String>> sekiller= new ArrayList<>();
    FirebaseDatabase db;
    public AllQuestion(){
        db= FirebaseDatabase.getInstance();
        DatabaseReference myRef =db.getReference().child("ogrenbolum").child("sekilvedesen");
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map<String,String> newPost = (Map)dataSnapshot.child("1").getValue();
                soruresmi.add(newPost.get("soruresmi"));
                soru.add(newPost.get("soru"));
                iste.add(newPost.get("istenilen"));
                sik1.add(newPost.get("sik1"));
                sik2.add(newPost.get("sik2"));
                sik3.add(newPost.get("sik3"));
                sik4.add(newPost.get("sik4"));
                cevap.add(newPost.get("sik5"));
                sekiller.add(soru);
                sekiller.add(iste);
                sekiller.add(soruresmi);
                sekiller.add(sik1);
                sekiller.add(sik2);
                sekiller.add(sik3);
                sekiller.add(sik4);
                sekiller.add(cevap);
                Log.e("Veri:",newPost.get("soru"));
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    //Şekiller ve Renkler
    public ArrayList<String> sekilverenk(int i){
       return  sekiller.get(i);
    }
}
