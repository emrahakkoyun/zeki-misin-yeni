package com.gojolo.zekimisin;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SosyalAdapter extends BaseAdapter{
    private Context mContext;
    List<String> mesaj = new ArrayList<String>();
    List<String> resim = new ArrayList<String>();
    List<String> tarih = new ArrayList<String>();
    List<String> profil = new ArrayList<String>();
    List<String> puan = new ArrayList<String>();
    List<String> usernamex = new ArrayList<String>();
    List<String> comment = new ArrayList<String>();
    List<String> like = new ArrayList<String>();
    public int who;

    public SosyalAdapter(Context c,List<String> mesaj,List<String> resim,List<String> tarih,List<String> profil,List<String> puan,List<String> username ,List<String> comment,List<String> like) {
        mContext = c;
        this.mesaj = mesaj;
        this.resim= resim;
        this.tarih=tarih;
        this.profil=profil;
        this.puan=puan;
        this.usernamex=username;
        this.comment=comment;
        this.like=like;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mesaj.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mesaj.indexOf(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid=convertView;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            grid = inflater.inflate(R.layout.grid_sosyal, null);

        }
        ImageView photo = (ImageView)grid.findViewById(R.id.photo);
        ImageView picture = (ImageView)grid.findViewById(R.id.picture);
        TextView score = (TextView) grid.findViewById(R.id.score);
        TextView usernametext = (TextView) grid.findViewById(R.id.username);
        TextView mesajtext = (TextView) grid.findViewById(R.id.mesajtext);
        TextView tarihtext = (TextView) grid.findViewById(R.id.tarihtext);
        TextView commentcounttext = (TextView) grid.findViewById(R.id.commentcount);
        TextView likecounttext = (TextView) grid.findViewById(R.id.likecount);
        Log.e("profil",profil.get(position));
        switch (profil.get(position)){
            case "1":{ photo.setImageResource(R.drawable.ph1); break;}
            case "2":{ photo.setImageResource(R.drawable.ph2); break;}
            case "3":{ photo.setImageResource(R.drawable.ph3); break;}
            case "4":{ photo.setImageResource(R.drawable.ph4); break;}
            case "5":{ photo.setImageResource(R.drawable.ph5); break;}
            case "6":{ photo.setImageResource(R.drawable.ph6); break;}
            case "7":{ photo.setImageResource(R.drawable.ph7); break;}
            case "8":{ photo.setImageResource(R.drawable.ph8); break;}
            case "9":{ photo.setImageResource(R.drawable.ph9); break;}
            case "10":{ photo.setImageResource(R.drawable.ph10); break;}
        }
        if(resim.get(position)==null){}else{
        Picasso.get().load(resim.get(position)).into(picture);}
        usernametext.setText(usernamex.get(position));
        score.setText(puan.get(position));
        mesajtext.setText(mesaj.get(position));
        tarihtext.setText(tarih.get(position));
        commentcounttext.setText(comment.get(position));
        likecounttext.setText(like.get(position));
        return grid;
    }
}