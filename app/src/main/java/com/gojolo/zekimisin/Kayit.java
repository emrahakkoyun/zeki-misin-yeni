package com.gojolo.zekimisin;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Kayit extends AppCompatActivity {
    EditText edt;
    Button grs;
    Intent in;
    String deviceId;
    FirebaseDatabase db;
    ImageView img1,img2,img3,img4,img5,img6,img7,img8,img9,img10;
    int profil=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kayit);
        edt = (EditText)findViewById(R.id.isim);
        grs  = (Button)findViewById(R.id.giris);
        img1=(ImageView)findViewById(R.id.im1);
        img2=(ImageView)findViewById(R.id.im2);
        img3=(ImageView)findViewById(R.id.im3);
        img4=(ImageView)findViewById(R.id.im4);
        img5=(ImageView)findViewById(R.id.im5);
        img6=(ImageView)findViewById(R.id.im6);
        img7=(ImageView)findViewById(R.id.im7);
        img8=(ImageView)findViewById(R.id.im8);
        img9=(ImageView)findViewById(R.id.im9);
        img10=(ImageView)findViewById(R.id.im10);
        img1.setBackgroundResource(R.drawable.ph1);
        img2.setBackgroundResource(R.drawable.ph2);
        img3.setBackgroundResource(R.drawable.ph3);
        img4.setBackgroundResource(R.drawable.ph4);
        img5.setBackgroundResource(R.drawable.ph5);
        img6.setBackgroundResource(R.drawable.ph6);
        img7.setBackgroundResource(R.drawable.ph7);
        img8.setBackgroundResource(R.drawable.ph8);
        img9.setBackgroundResource(R.drawable.ph9);
        img10.setBackgroundResource(R.drawable.ph10);
        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profil=1;
                resimayarla(1);
            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profil=2;
                resimayarla(2);
            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profil=3;
                resimayarla(3);
            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profil=4;
                resimayarla(4);
            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profil=5;
                resimayarla(5);
            }
        });
        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profil=6;
                resimayarla(6);
            }
        });
        img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profil=7;
                resimayarla(7);
            }
        });
        img8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profil=8;
                resimayarla(8);
            }
        });
        img9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profil=9;
                resimayarla(9);
            }
        });
        img10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profil=10;
                resimayarla(10);
            }
        });
        db= FirebaseDatabase.getInstance();
        FirebaseApp.initializeApp(this);
        deviceId= Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        grs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edt.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Kullanıcı Adınızı Giriniz",Toast.LENGTH_LONG).show();
                }
                else if(profil==0){
                    Toast.makeText(getApplicationContext(),"Profil Resmi Seçiniz",Toast.LENGTH_LONG).show();
                }
                else{
                    usernameadd(edt.getText().toString(),profil);
                }
            }
        });

    }
    public void resimayarla(int i){
     switch (i) {
         case 1: {
             img1.setBackgroundResource(R.drawable.ph1s);
             img2.setBackgroundResource(R.drawable.ph2);
             img3.setBackgroundResource(R.drawable.ph3);
             img4.setBackgroundResource(R.drawable.ph4);
             img5.setBackgroundResource(R.drawable.ph5);
             img6.setBackgroundResource(R.drawable.ph6);
             img7.setBackgroundResource(R.drawable.ph7);
             img8.setBackgroundResource(R.drawable.ph8);
             img9.setBackgroundResource(R.drawable.ph9);
             img10.setBackgroundResource(R.drawable.ph10);
             break;
         }
         case 2: {
             img1.setBackgroundResource(R.drawable.ph1);
             img2.setBackgroundResource(R.drawable.ph2s);
             img3.setBackgroundResource(R.drawable.ph3);
             img4.setBackgroundResource(R.drawable.ph4);
             img5.setBackgroundResource(R.drawable.ph5);
             img6.setBackgroundResource(R.drawable.ph6);
             img7.setBackgroundResource(R.drawable.ph7);
             img8.setBackgroundResource(R.drawable.ph8);
             img9.setBackgroundResource(R.drawable.ph9);
             img10.setBackgroundResource(R.drawable.ph10);
             break;
         }
         case 3: {
             img1.setBackgroundResource(R.drawable.ph1);
             img2.setBackgroundResource(R.drawable.ph2);
             img3.setBackgroundResource(R.drawable.ph3s);
             img4.setBackgroundResource(R.drawable.ph4);
             img5.setBackgroundResource(R.drawable.ph5);
             img6.setBackgroundResource(R.drawable.ph6);
             img7.setBackgroundResource(R.drawable.ph7);
             img8.setBackgroundResource(R.drawable.ph8);
             img9.setBackgroundResource(R.drawable.ph9);
             img10.setBackgroundResource(R.drawable.ph10);
             break;
         }
         case 4: {
             img1.setBackgroundResource(R.drawable.ph1);
             img2.setBackgroundResource(R.drawable.ph2);
             img3.setBackgroundResource(R.drawable.ph3);
             img4.setBackgroundResource(R.drawable.ph4s);
             img5.setBackgroundResource(R.drawable.ph5);
             img6.setBackgroundResource(R.drawable.ph6);
             img7.setBackgroundResource(R.drawable.ph7);
             img8.setBackgroundResource(R.drawable.ph8);
             img9.setBackgroundResource(R.drawable.ph9);
             img10.setBackgroundResource(R.drawable.ph10);
             break;
         }
         case 5: {
             img1.setBackgroundResource(R.drawable.ph1);
             img2.setBackgroundResource(R.drawable.ph2);
             img3.setBackgroundResource(R.drawable.ph3);
             img4.setBackgroundResource(R.drawable.ph4);
             img5.setBackgroundResource(R.drawable.ph5s);
             img6.setBackgroundResource(R.drawable.ph6);
             img7.setBackgroundResource(R.drawable.ph7);
             img8.setBackgroundResource(R.drawable.ph8);
             img9.setBackgroundResource(R.drawable.ph9);
             img10.setBackgroundResource(R.drawable.ph10);
             break;
         }
         case 6: {
             img1.setBackgroundResource(R.drawable.ph1);
             img2.setBackgroundResource(R.drawable.ph2);
             img3.setBackgroundResource(R.drawable.ph3);
             img4.setBackgroundResource(R.drawable.ph4);
             img5.setBackgroundResource(R.drawable.ph5);
             img6.setBackgroundResource(R.drawable.ph6s);
             img7.setBackgroundResource(R.drawable.ph7);
             img8.setBackgroundResource(R.drawable.ph8);
             img9.setBackgroundResource(R.drawable.ph9);
             img10.setBackgroundResource(R.drawable.ph10);
             break;
         }
         case 7: {
             img1.setBackgroundResource(R.drawable.ph1);
             img2.setBackgroundResource(R.drawable.ph2);
             img3.setBackgroundResource(R.drawable.ph3);
             img4.setBackgroundResource(R.drawable.ph4);
             img5.setBackgroundResource(R.drawable.ph5);
             img6.setBackgroundResource(R.drawable.ph6);
             img7.setBackgroundResource(R.drawable.ph7s);
             img8.setBackgroundResource(R.drawable.ph8);
             img9.setBackgroundResource(R.drawable.ph9);
             img10.setBackgroundResource(R.drawable.ph10);
             break;
         }
         case 8: {
             img1.setBackgroundResource(R.drawable.ph1);
             img2.setBackgroundResource(R.drawable.ph2);
             img3.setBackgroundResource(R.drawable.ph3);
             img4.setBackgroundResource(R.drawable.ph4);
             img5.setBackgroundResource(R.drawable.ph5);
             img6.setBackgroundResource(R.drawable.ph6);
             img7.setBackgroundResource(R.drawable.ph7);
             img8.setBackgroundResource(R.drawable.ph8s);
             img9.setBackgroundResource(R.drawable.ph9);
             img10.setBackgroundResource(R.drawable.ph10);
             break;
         }
         case 9: {
             img1.setBackgroundResource(R.drawable.ph1);
             img2.setBackgroundResource(R.drawable.ph2);
             img3.setBackgroundResource(R.drawable.ph3);
             img4.setBackgroundResource(R.drawable.ph4);
             img5.setBackgroundResource(R.drawable.ph5);
             img6.setBackgroundResource(R.drawable.ph6);
             img7.setBackgroundResource(R.drawable.ph7);
             img8.setBackgroundResource(R.drawable.ph8);
             img9.setBackgroundResource(R.drawable.ph9s);
             img10.setBackgroundResource(R.drawable.ph10);
             break;
         }
         case 10: {
             img1.setBackgroundResource(R.drawable.ph1);
             img2.setBackgroundResource(R.drawable.ph2);
             img3.setBackgroundResource(R.drawable.ph3);
             img4.setBackgroundResource(R.drawable.ph4);
             img5.setBackgroundResource(R.drawable.ph5);
             img6.setBackgroundResource(R.drawable.ph6);
             img7.setBackgroundResource(R.drawable.ph7);
             img8.setBackgroundResource(R.drawable.ph8);
             img9.setBackgroundResource(R.drawable.ph9);
             img10.setBackgroundResource(R.drawable.ph10s);
             break;
         }
     }
    }
    public void  usernameadd(String username,int profil){
        DatabaseReference dbRef=db.getReference("user");
        DatabaseReference dbRefYeni=db.getReference("user/"+deviceId+"/username");
        dbRefYeni.setValue(username);
        DatabaseReference dbRefYenix=db.getReference("user/"+deviceId+"/profil");
        dbRefYenix.setValue(profil);
        DatabaseReference dbRefYenixx=db.getReference("user/"+deviceId+"/puan");
        dbRefYenixx.setValue(0);
        in = new Intent(Kayit.this,BeforeHome.class);
        startActivity(in);

    }
}
