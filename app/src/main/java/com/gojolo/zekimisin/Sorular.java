package com.gojolo.zekimisin;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

public class Sorular extends Fragment {
    public String[] yazilar = {
            "Zeka Soruları",
            "Mantık Soruları",
            "İşlem Soruları",
            "Hafıza Soruları",
            "Şaşırtmaca Sorular",
    };
    public Integer[] resimler = {
            R.drawable.slogin,
            R.drawable.slogin,
            R.drawable.slogin,
            R.drawable.slogin,
            R.drawable.slogin,
            R.drawable.slogin,
            R.drawable.slogin,
            R.drawable.slogin,

    };
    View view;
    Intent intent;
    GridView grid;
    ImageAdapter adapter;
    public static Sorular newInstance() {
        Sorular fragment = new Sorular();
        return fragment;
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sorular, container, false);

        adapter = new ImageAdapter(getActivity(), yazilar, resimler,2);
        grid=(GridView)view.findViewById(R.id.grid);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                inrequestadd(position);
                }
        });
                return view;
    }
    public void inrequestadd(final int position) {
        intent = new Intent(getActivity(),test2.class);
        intent.putExtra("id", position);
        startActivity(intent);
    }
}
