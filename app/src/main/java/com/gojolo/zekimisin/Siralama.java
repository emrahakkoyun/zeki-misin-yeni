package com.gojolo.zekimisin;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Siralama extends Fragment {

    View view;
    GridView grid;
    FirebaseDatabase db;

    List<String> fprofil = new ArrayList<String>();
    List<String> fpuan = new ArrayList<String>();
    List<String> fusername = new ArrayList<String>();
    List<String> fsiralama = new ArrayList<String>();
    int i=0;
    SiralamaAdapter adapter;
    public static Siralama newInstance() {
        Siralama fragment = new Siralama();
        return fragment;
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_siralama, container, false);
        db= FirebaseDatabase.getInstance();
        grid =(GridView)view.findViewById(R.id.grid);
        DatabaseReference dbRefYeni =db.getReference("user");
        dbRefYeni.orderByChild("puan").limitToLast(30).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot edataSnapshot : dataSnapshot.getChildren()) {
                    i++;
                    fprofil.add(edataSnapshot.child("profil").getValue().toString());
                    fpuan.add(edataSnapshot.child("puan").getValue().toString());
                    fusername.add(edataSnapshot.child("username").getValue().toString());
                    fsiralama.add(String.valueOf(i));
                    Collections.reverse(fprofil);
                    Collections.reverse(fpuan);
                    Collections.reverse(fusername);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        adapter = new SiralamaAdapter(getActivity(), fprofil,fpuan,fusername,fsiralama);
        grid.setAdapter(adapter);
        return view;
    }
}
