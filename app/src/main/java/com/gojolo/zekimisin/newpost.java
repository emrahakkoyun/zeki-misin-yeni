package com.gojolo.zekimisin;

import android.content.ContentResolver;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;

public class newpost extends DialogFragment implements TextView.OnEditorActionListener {

    public interface EditNameDialogListener {
        void onFinishEditDialog(String inputText);
    }
    private static final int PICK_IMAGE_REQUEST=1;
    private Uri mImageUri;
    private EditText mEditText;
    private ImageView photo;
    private TextView username,score;
    private ImageButton send,getpic;
    private ImageView imgshow;
    String deviceId;
    String fphoto,fpuan,fusername;
    FirebaseDatabase db;
    FirebaseStorage storage;
    StorageReference storageReference;
    String imaglink;
    Boolean resimsecildi=false;
    public newpost() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_name, container);
        db= FirebaseDatabase.getInstance();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        deviceId= Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        photo = (ImageView) view.findViewById(R.id.photo);
        username = (TextView) view.findViewById(R.id.username);
        score = (TextView) view.findViewById(R.id.score);
        imgshow=(ImageView)view.findViewById(R.id.imgshow);
        send=(ImageButton)view.findViewById(R.id.send);
        getpic=(ImageButton)view.findViewById(R.id.getpic);
        getpic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });
        DatabaseReference dbRefYeni =db.getReference("user/"+deviceId);
        dbRefYeni.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String , String> mapa = (Map)dataSnapshot.getValue();
                fphoto=String.valueOf(mapa.get("profil"));
                fpuan=String.valueOf(mapa.get("puan"));
                fusername=String.valueOf(mapa.get("username"));
                addsharedialog(fphoto,fpuan,fusername);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mesaj=mEditText.getText().toString();
                uploadFile();
                if(fusername!=null&&resimsecildi==false) {
                    messagesend(mesaj,fusername,imaglink,fpuan,fphoto);
                }
            }
        });
        mEditText = (EditText) view.findViewById(R.id.txt_your_name);
        getDialog().setTitle("Hello");

        // Show soft keyboard automatically
        mEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        mEditText.setOnEditorActionListener(this);

        return view;
    }
    public void addsharedialog(String fphoto,String fpuan,String fusername){
        switch (fphoto){
            case "1":{ photo.setImageResource(R.drawable.ph1); break;}
            case "2":{ photo.setImageResource(R.drawable.ph2); break;}
            case "3":{ photo.setImageResource(R.drawable.ph3); break;}
            case "4":{ photo.setImageResource(R.drawable.ph4); break;}
            case "5":{ photo.setImageResource(R.drawable.ph5); break;}
            case "6":{ photo.setImageResource(R.drawable.ph6); break;}
            case "7":{ photo.setImageResource(R.drawable.ph7); break;}
            case "8":{ photo.setImageResource(R.drawable.ph8); break;}
            case "9":{ photo.setImageResource(R.drawable.ph9); break;}
            case "10":{ photo.setImageResource(R.drawable.ph10); break;}
        }
        username.setText(fusername);
        score.setText(fpuan + " puan");

    }
    public void messagesend(String mesaj,String gfusername,
    String imaglinkx,String puan,String profil){
        DatabaseReference dbRef=db.getReference("sosyal");
        String key= dbRef.push().getKey();
        DatabaseReference dbRefYenixx=db.getReference("sosyal/"+key+"/username");
        dbRefYenixx.setValue(gfusername);
        DatabaseReference dbRefYeniprofil=db.getReference("sosyal/"+key+"/profil");
        dbRefYeniprofil.setValue(profil);
        DatabaseReference dbRefYenipuan=db.getReference("sosyal/"+key+"/puan");
        dbRefYenipuan.setValue(puan);
        DatabaseReference dbRefYeni=db.getReference("sosyal/"+key+"/mesaj");
        dbRefYeni.setValue(mesaj);
        DatabaseReference dbRefYeniy=db.getReference("sosyal/"+key+"/resim");
        dbRefYeniy.setValue(imaglinkx);
        DatabaseReference dbRefYenixxxe=db.getReference("sosyal/"+key+"/comment");
        dbRefYenixxxe.setValue("0");
        DatabaseReference dbRefYenixxx=db.getReference("sosyal/"+key+"/like");
        dbRefYenixxx.setValue("0");
        DatabaseReference dbRefYenix=db.getReference("sosyal/"+key+"/tarih");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String strDate = dateFormat.format(date).toString();
        dbRefYenix.setValue(strDate);
        dismiss();
    }
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (EditorInfo.IME_ACTION_DONE == actionId) {
            // Return input text to activity
            EditNameDialogListener activity = (EditNameDialogListener) getActivity();
            activity.onFinishEditDialog(mEditText.getText().toString());
            this.dismiss();
            return true;
        }
        return false;
    }
    public void openFileChooser(){
        resimsecildi=true;
        Intent in = new Intent();
        in.setType("image/*");
        in.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(in,PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            mImageUri=data.getData();
            imgshow.getLayoutParams().height = 150;
            imgshow.getLayoutParams().width = 150;
            imgshow.setScaleType(ImageView.ScaleType.FIT_XY);
            Picasso.get().load(mImageUri).into(imgshow);
        }
    }
    private void uploadFile() {
        final String mesaj=mEditText.getText().toString();
        dismiss();
        if(mImageUri!= null)
        {

           final StorageReference ref = storageReference.child("images/"+ UUID.randomUUID().toString());
            ref.putFile(mImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                            while (!urlTask.isSuccessful());
                            Uri downloadUrl = urlTask.getResult();
                            Log.e("veri",downloadUrl.toString());
                            imaglink=downloadUrl.toString();
                            messagesend(mesaj,fusername,imaglink,fpuan,fphoto);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        }
                    });
        }
    }
}