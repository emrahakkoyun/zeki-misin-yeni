package com.gojolo.zekimisin;

class Post {
    public String mesaj;
    public String tarih;
    public String resim;
    public String username;
    public String puan;
    public String profil;
    public String like;
    public String comment;
    public Post(){

    }
    public String getUsername() {
        return username;
    }

    public String getTarih() {
        return tarih;
    }

    public String getResim() {
        return resim;
    }

    public String getPuan() {
        return puan;
    }

    public String getProfil() {
        return profil;
    }

    public String getMesaj() {
        return mesaj;
    }

    public String getComment() {
        return comment;
    }

    public String getLike() {
        return like;
    }

    public Post(String mesaj, String tarih, String resim, String username, String puan, String profil, String like, String comment){
        this.mesaj=mesaj;
        this.tarih=tarih;
        this.resim=resim;
        this.username=username;
        this.puan=puan;
        this.profil=profil;
        this.like=like;
        this.comment=comment;
    }
}
