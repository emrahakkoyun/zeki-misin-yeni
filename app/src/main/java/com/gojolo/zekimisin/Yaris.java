package com.gojolo.zekimisin;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cunoraz.gifview.library.GifView;

public class Yaris extends Fragment {

    View view;
    public static Yaris newInstance() {
        Yaris fragment = new Yaris();
        return fragment;
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_yaris, container, false);

        return view;
    }
}
