package com.gojolo.zekimisin;

        import android.app.FragmentManager;
        import android.os.Bundle;
        import android.support.annotation.NonNull;
        import android.support.design.widget.BottomNavigationView;
        import android.support.v4.app.Fragment;
        import android.support.v4.app.FragmentTransaction;
        import android.support.v7.app.AppCompatActivity;
        import android.view.MenuItem;
        import android.view.View;
        import android.widget.ImageView;
        import android.widget.TextView;
        import android.widget.Toast;

public class Home extends AppCompatActivity{
    android.support.v4.app.FragmentManager fm = getSupportFragmentManager();

    SosyalContent sosyalc = (SosyalContent) fm.findFragmentById(R.id.sosyalcontent);
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.egzersiz:
                    fragment = new Ogren();
                    loadFragment(fragment);
                    return true;
                case R.id.sorular:
                    fragment = new Sorular();
                    loadFragment(fragment);
                    return true;
                case R.id.siralama:
                    fragment  = new Siralama();
                    loadFragment(fragment);
                    return true;
                case R.id.sosyal:
                    fragment = new Sosyal();
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }
    };
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ImageView yaris=(ImageView)findViewById(R.id.yaris);
        yaris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new Yaris());
            }
        });
        BottomNavigationView bnav = (BottomNavigationView) findViewById(R.id.bottomnav);
        BottomNavigationView bnav2 = (BottomNavigationView) findViewById(R.id.bottomnav2);
        BottomNavigationViewHelper.disableShiftMode(bnav);
        BottomNavigationViewHelper.disableShiftMode(bnav2);
       bnav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        bnav2.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        loadFragment(new Ogren());
    }
}
